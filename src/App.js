import React, { Component } from 'react';
import './App.css';
import {inject} from 'mobx-react';

@inject('exampleStore')
class App extends Component {
  render() { 
    return ( <div> Hello World </div> );
  }
}

export default App;
